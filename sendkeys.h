/*$Id: sendkeys.h,v 1.6 2003-06-19 16:21:01-04 maxwell Exp maxwell $
"c:\program files\lcc\bin\make.exe" all
*/

#ifndef _INC_SENDKEYS
#define _INC_SENDKEYS

//-----------------------Function Prototypes-------------------

VOID ZeroCounts(VOID);
VOID SetKeyState(UINT iVirtualKey, UINT iState);
VOID SendKeys(UINT iVirtualKey, BOOL bShifted);
BOOL HasWindowName(VOID);
BOOL IsMatchingWindowName(LPCSTR lpszWinName);
__declspec(dllexport) DWORD FAR HKKeyHook(int nCode, WORD wParam, DWORD lParam);


//-----------------------Constants--------------------------
#define MOST_SIG_BIT 0x80000000  //For DWORDs
#define CLASSNAMESIZE 32  //Size of Buffer used for return of WinAPI GetClassName()

#define KEY_UP   KEYEVENTF_KEYUP  //KEYEVENTF_KEYUP is defined in WINUSER.H
#define KEY_DOWN 0

//The ff. don't seem to be defined in WINUSER.H:
#define VK_OPEN_BRACKET  219     // '['  VK_OEM_4
#define VK_CLOSE_BRACKET 221     // ']'  VK_OEM_6
#define VK_BACKSLASH     220     // '\' VK_OEM_5
#define VK_A             0x41
#define VK_B             0x42
#define VK_C             0x43
#define VK_D             0x44
#define VK_E             0x45
#define VK_F             0x46
#define VK_G             0x47
#define VK_H             0x48
#define VK_I             0x49
#define VK_J             0x4A
#define VK_K             0x4B
#define VK_L             0x4C
#define VK_M             0x4D
#define VK_N             0x4E
#define VK_O             0x4F
#define VK_P             0x50
#define VK_Q             0x51
#define VK_R             0x52
#define VK_S             0x53
#define VK_T             0x54
#define VK_U             0x55
#define VK_V             0x56
#define VK_W             0x57
#define VK_X             0x58
#define VK_Y             0x59
#define VK_Z             0x5A

#define  ALT_MASK    4
#define  CTRL_MASK   2
#define  SHIFT_MASK  1

//Whether a keystroke is to be shifted:
#define SHIFTED 1
#define UNSHIFTED 0

//Whether the original keystroke is to remain visible in the queue or not:
#define VISIBLE_KEYSTROKE    0L
#define INVISIBLE_KEYSTROKE  1L

/*Strings returned by GetClassName(), and used by IsMatchingWindowName()
  to identify the app in question.  The following apps all misbehave in
  some way or other, and are thus special cases.*/
#define EXCEL_MAIN_NAME (LPCSTR) "EXCEL7"           //Excel (main pane)
#define WORD_NAME       (LPCSTR) "_WwG"             //Word97 or Word2K
#define REMOTE_NAME     (LPCSTR) "IHWindowClass"    /*Remote Desktop messes up
  many keystrokes, so run this keystroke mapper inside RD and don't try to
  translate the keys here*/

#endif  /* _INC_SENDKEYS */

