### What is this repository for? ###

This creates a Windows tray app that allows the use of alphabetic or numeric
control keys to control the cursor in nearly all Windows apps, allowing your
fingers to remain on the main keyboard, rather than having to shift over to
your keypad or somewhere equally inconvenient.  My chosen control keys are hard
coded in the file sendkeys.c, but you can of course change the assignment of
keys by editing that file.  You could probably also remap Alt keys, shifted
control keys, etc., although I have not tried that.

Notice that this goes beyond the capabilities of any other key remapping program
that I know of, although I'd be happy to find out I'm wrong.  For example, most
key remapping programs allow you to define one input key (or sometimes a chord)
as equivalent to a single output keystroke.  My program does not have that
limitation, for example ^D remaps to seven down arrows, so it moves the cursor
down seven rows.

Perhaps more interesting, ^Q is set to toggle selection; so while ^H normally
outputs a single left arrow keystroke, when selection is toggled on, ^H outputs
a single *shifted* left arrow keystroke, meaning the character which was to
the left of the cursor becomes selected.  Selection can be turned off by a
second ^Q, or by a ^C (copy, of course) or ^X.  If no text is selected, ^C
attempts to copy the word the cursor is in, and analogously for ^E.

My mapping causes ^A to go to the beginning of the line the first time it is hit
(i.e. it outputs the <HOME> keystroke).  The second time ^A is typed (assuming
no other keystrokes in between), it will jump to the top of the current screen
in most apps; and the third time, it should jump to the beginning of the file.
Analogous with ^E (<END>).  However, some apps will break this behavior, e.g.
most browsers will do something like going to the first tab the third time you
hit ^A.

Right clicking on the tray icon will bring up an old-fashioned dialog box, which
allows you to toggle CtrlKeys off and on; this may come in useful if you need
for example to use ^C to kill a process in a terminal, rather than to copy text.
You can also exit CtrlKeys from that menu.


### How do I get set up? ###

The makefile is set up to compile the code using the free C compiler in Msys64
(https://www.msys2.org).  But you can probably replace that assignment with
other C compilers for Windows.

Once you have compiled the CtrlKeys.exe and sendkeys.dll files, you should be
able to launch it.  On my PC, I have a shortcut to CtrlKeys.exe in my Startup
folder, so it automatically starts when I log in to Windows.


### Limitations ###

This is tested with Windows 10, and previous versions ran fine on earlier
versions of Windows, all the way back to Windows 3.1 (although the current
version would of course not run on those ancient versions).  I *suspect* the
current version would run under Windows 7 and possibly XP, but I have not tested
that.

Microsoft Edge breaks this, in the sense that key remapping will not work in
Edge's address bar (and probably not in other places either, although I have
not experimented with this).  Microsoft Word and Excel have their own
peculiarities, for which see the discussion of ^A in sendkeys.c; similarly for
browsers, if you type ^A three times in a row, something funny will usually
happen.

If you are running Windows Remote Desktop in full screen mode, it will not pass
the re-mapped keys at all--apparently RD simply takes over the keyboard, and no
remapping happens.  When running Remote Desktop in non-fullscreen mode, some
keys get remapped, while others do not.  (In particular, ^Q is unable to toggle
the selection mode.)  Since this is so confusing, sendkeys looks at the name of
the window that has keyboard focus; if it's Remote Desktop, sendkeys does nothing
at all, passing the original control keys on to the remote.  I therefore run
a separate instance of CtrlKeys on the remote desktop, and everything works as
it should.

I would have thought that doing something similar in Linux would be easy.  But
as far as I know, nothing like this exists in the Linux world--there are of
course keyboard remappers there, but nothing that I have seen that allows one
key to output a sequence of other keystrokes, nor that allows the sort of
toggling I do with ^Q.  Again, I would like to be proven wrong.


### Contribution guidelines ###

I suppose the use of C to create a Windows tray app is outdated.  I have not
however tried to build the tray app using C#, although that might be a good
addition.  Afaict, using C# requires the use of Microsoft's Visual Studio (not
to be confused with Visual Studio Code), and I have not installed that on my
computer.  That might be the easiest way to add a popup menu to the tray app,
rather than the ancient popup dialog box that I use now.


### Who do I talk to? ###

My email:  mmaxwell@umd.edu


### Where did this come from? ###

The original version of this program came from Jeffrey Richter's article
published in the Microsoft Systems Journal, Dec. 1992, p43.  His version (which
ran under Windows 3.1) implemented the diamond-style keystroke remapping that
had been popularized by Wordstar, back in the MS-DOS era.  My fingers were more
familiar with the keystrokes of the vi (now vim) program.  (vi(m) is of course
a modal program, but since vi is probably the only modal program most of us have
access to, I changed the ordinary hjkl etc. keys to be control keys instead.
My fingers were still comfortable.)

I made lots more changes as the years--and the versions of Windows--went by, so
very little of Jeffrey's original code remains.  The biggest change was of course
going from 16-bit code to 32-bit code.  I corresponded with Jeffrey back in May
2006 concerning any copyright issues, and he wrote back to tell me "I don't care
what happens to my 16-bit code anymore."  So I believe it is safe for me to
redistribute this code.