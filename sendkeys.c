/*sendkeys.c
  Remap keys (particularly control keys) into cursor movement keys, etc.
*/

#include <Windows.h>  //This in turn loads Winuser.h
#include "sendkeys.h"

//Application global variables (all start with "g_").
static BOOL       g_bAnchorDropped = FALSE; //^Q toggles this, and ^C, ^G, ^X turn it off,
                                            // as do any keys that we do not xlate (incl. ALT)
static int g_iHomeCount;
static int g_iEndCount;

VOID ZeroCounts()
/*Zero out the global vars that keep track of Home, End and Copy keypresses.
*/
{
  g_iHomeCount = 0;
  g_iEndCount = 0;
  return;
}

VOID SetKeyState(UINT iVirtualKey, UINT iState)
/*Set the specified key to the specified state (KEY_UP or KEY_DOWN).
*/
{
   DWORD dwFlags;
   UINT  iScanCode;

   dwFlags = (DWORD) iState;
      //DWORD is an unsigned 32-bit int, UINT is an unsigned int
   iScanCode = MapVirtualKey(iVirtualKey, 0);
   if (iScanCode & 0xE0)
      dwFlags |= KEYEVENTF_EXTENDEDKEY;
   keybd_event((BYTE) iVirtualKey, (BYTE) iScanCode, dwFlags, 0);
      /*For some reason, keybd_event wants its scan code arg to be a byte.
        The cast wipes out any info on whether the iScanCode marked
        an extended key, but we've already gotten that info into dwFlags.*/
   return;
}


VOID SendKey(UINT iVirtualKey, BOOL bShifted, BOOL bKeyDown)
/*Send KeyUp or KeyDown msg for the specified virtual keycode, together with its
  scan code.  If bShifted is TRUE, we bracket those msgs with down/up msgs for
  the shift key.  bKeyDown is TRUE if this was a keydown event, else false.
*/
{
   if (bShifted)
     SetKeyState(VK_SHIFT, KEY_DOWN);
   if (bKeyDown)
     SetKeyState(iVirtualKey, KEY_DOWN);
   else
     SetKeyState(iVirtualKey, KEY_UP);
   if (bShifted)
     SetKeyState(VK_SHIFT, KEY_UP);
   return;
}

BOOL HasWindowName(VOID)
/*Return TRUE if the window where keyboard input is going has a class name,
  FALSE if it does not.  (In earlier versions of Windows, the command prompt and
  a few other programs do not return a class.  This no longer seems to be the case,
  so this function is probably irrelevant now.)
*/
{
  char lpszClassName[CLASSNAMESIZE];

  if (GetClassName(GetFocus(), (LPTSTR) lpszClassName, CLASSNAMESIZE))
    //the return value is the number of chars in the class name; we just want to return TRUE:
    return TRUE;
  else
    return FALSE;
}


BOOL IsMatchingWindowName(LPCSTR lpszWinName)
/*Return TRUE if the name of the class of the window where keyboard input is going
  matches the given name, else FALSE if it does not, or if the window does not
  have a class.  (The command prompt, the bash shell, and probably other
  such programs do not return a class.)
*/
{
   char lpszClassName[CLASSNAMESIZE];
   int iClassNameRetValue;

   iClassNameRetValue = GetClassName(GetFocus(), (LPTSTR) lpszClassName, CLASSNAMESIZE);
#if 0
   /*Set above to 1 to determine a Window class name (this works when
     this function is called, which only happens with some keys, e.g. ^C)*/
   if (iClassNameRetValue != 0)
     MessageBoxEx(NULL, lpszClassName, "Window Class Name", MB_OK, LANG_ENGLISH);
   else
     //Command prompt (and bash shell) does not return a value
     MessageBoxEx(NULL, "No value", "Window Class Name", MB_OK, LANG_ENGLISH);
#endif
   if (iClassNameRetValue == 0)
      //Command prompt (and bash shell) does not return a value
      return FALSE;
   //We get here if GetClassName() returned a value, meaning the window had a class name
   else if (lstrcmp((LPCSTR) lpszClassName, lpszWinName))
           //The class name did NOT match the arg (lstrcmp returns 0 if the strings match)
           return FALSE;
        else
           //The class name DID match the arg
           return TRUE;
}


__declspec(dllexport) DWORD FAR HKKeyHook(int nCode, WORD wVirtKeyCode, DWORD lKeystrokeInfo)
/*Translate the user's keystrokes into the desired output keys.
  Returning INVISIBLE_KEYSTROKE (1L) results in the key that was read NOT being
  seen by the app; returning VISIBLE_KEYSTROKE (0L) results in the key's being
  seen, in addition to whatever SendKey sends.
  According to the WinHelp on KeyboardProc, if nCode = HC_ACTION (0), it's
  an ordinary keyboard msg; if nCode = HC_NOREMOVE (3), it's a keyboard msg,
  but an application called the PeekMessage function, specifying the PM_NOREMOVE
  flag.  (Word and Mozilla TBird seem to do this, most other apps do not.)
  But if nCode is less than zero, this hook procedure must pass the message to
  the CallNextHookEx function without further processing, and return the value
  returned by CallNextHookEx.  In any case, wVirtKeyCode (wParam) contains
  a virtual-key code, and lKeystrokeInfo (lParam) specifies the repeat count,
  scan code, extended-key flag, context code, previous key-state flag, and
  transition-state flag.
*/
{
  int iShiftStates;
  int iVirtKeyCode;
  BOOL bKeyDown;  //True if this was KeyDown, False if it was KeyUp.

  if (nCode >= 0) //See explanation above of meaning of nCode
    {
    if (  (wVirtKeyCode == VK_CONTROL)
       || (wVirtKeyCode == VK_LCONTROL)
       || (wVirtKeyCode == VK_SHIFT))
      /*Ignore, in particular do not re-set any counts.  Note that VK_SHIFT is
        generated by the code here if the "anchor" is down.*/
      return VISIBLE_KEYSTROKE;

    //Determine status of Shift, Control and Alt keys:
    iShiftStates = 0;
    if (GetKeyState(VK_SHIFT) & 0x8000)
      /*If most significant bit is set in return from GetAsyncKeyState(),
        SHIFT key was down*/
      iShiftStates |= SHIFT_MASK;
    if ((GetKeyState(VK_LCONTROL) & 0x8000) || (GetKeyState(VK_CONTROL) & 0x8000))
      /*If most significant bit is set in return from GetAsyncKeyState(),
        _Left_ CTRL key was down.  (English keyboard which swaps CapsLock and
        LeftControl key does _not_ set VK_CONTROL, only VK_LCONTROL; but it does
        appear to get set when some programs are running, e.g. Mozilla Thunderbird.)
      */
      iShiftStates |= CTRL_MASK;
    if (GetKeyState(VK_MENU) & 0x8000)
      /*If most significant bit is set in return from GetAsyncKeyState(),
        ALT key was down*/
      iShiftStates |= ALT_MASK;

    if (((iShiftStates & CTRL_MASK) == CTRL_MASK)  /*Only map keys if CONTROL KEY is down*/
       && (IsMatchingWindowName(REMOTE_NAME) == FALSE))  /*...and we're not talking
                                                           to the Remote Desktop*/
      {
      /*It might seem we could set the Ctrl key status to "up" here.  But if we
        do that, when the msg for the actual char (A, B...) comes, then the Ctrl
        key is up, and so it never gets through the above 'if'.  So each 'case'
        has to do it individually (except those that don't want the ctrl key
        turned off, like ^B and ^W).*/
      bKeyDown = !(lKeystrokeInfo & MOST_SIG_BIT);
         /*Using lKeystrokeInfo seems to work better than GetKeyState or
           GetAsyncKeyState--latter two don't seem to see repeats when you hold
           key down.  Also, using MS-supplied KF_FLAG in place of MOST_SIG_BIT
           does not seem to work, as their flag seems to be a 16-bit value,
           rather than a 32-bit value.*/

      switch (wVirtKeyCode)
        {
        case VK_A: // ^A = beginning of line; xlate to:
                   // PgUp first time, ^PgUp second time, and ^Home third time
          {
          if (bKeyDown)
             {
             /*Alter the counts on bKeyDown only*/
             ++g_iHomeCount;
             g_iEndCount = 0;
             }
          if (g_iHomeCount == 1)
             {
             SetKeyState(VK_LCONTROL, KEY_UP);
             SendKey(VK_HOME, g_bAnchorDropped, bKeyDown);
             SetKeyState(VK_LCONTROL, KEY_DOWN);
             }
          else if (g_iHomeCount == 2)
             {
             //Leave Control key down, so we go to top of screen
             SendKey(VK_PRIOR, g_bAnchorDropped, bKeyDown);
                /*^PG_UP goes to top of screen in many apps.  However, in Word
                  this needs to be assigned to StartOfWindow (and shift ^PG_UP
                  to StartOfWindowExtend), using the Tools | Customize menu item.*/
             }
          else if (g_iHomeCount > 2) //Should never be > 3, but...
             {//Leave Control key down, so we go to TOF
             g_iHomeCount = 0;
             SendKey(VK_HOME, g_bAnchorDropped, bKeyDown);
             }
          return INVISIBLE_KEYSTROKE;
          }

        case VK_B: // ^B = word left
          ZeroCounts();
          SendKey(VK_LEFT, g_bAnchorDropped, bKeyDown);
          return INVISIBLE_KEYSTROKE;

        case VK_C: /*^C = copy slxn; if nothing is selected, we try to copy
                     the current word (= move one char right, in case cursor is
                     to left of first char of word; then go to beginning of word;
                     then select to end of word).  Outputting ^C in response to
                     ^C often causes an infinite loop, which no amount of
                     finagling seems to fix for every app.  So instead we output
                     a ^INSERT, which most apps appear to interpret the same way.
                   */
          ZeroCounts();
          //Special cases first:
          if (IsMatchingWindowName(EXCEL_MAIN_NAME)) //Excel (main pane, no slxn))
            {/*If we're talking to the main pane of Excel, let it see the ^C so
               it can copy the cell (otherwise we end up with a slxn that spans
               cells, and Excel refuses to copy it).  However, Excel's response
               seems to depend on whether there's a slxn, which we can't sense.
               And the Excel type-in pane goes into an infinite loop.*/
             g_bAnchorDropped = FALSE;
             return VISIBLE_KEYSTROKE;
            }
          /*If we get here, this is not Excel, so process the keystroke.*/
          if (g_bAnchorDropped)
            {//There is a slxn, so copy it:
            g_bAnchorDropped = FALSE;
            if (bKeyDown)
              {
              SendKey(VK_INSERT, UNSHIFTED, KEY_DOWN);
              SendKey(VK_INSERT, UNSHIFTED, KEY_UP);
              }
            return INVISIBLE_KEYSTROKE;
            }
          else
            {//If we get here, there is no current slxn, so select and copy
            // the current word instead:
            if (bKeyDown)
               {//<right><ctrl-left><ctrl-shift-right><ctrl-INS>
               SetKeyState(VK_LCONTROL, KEY_UP);
               SendKey(VK_RIGHT,  UNSHIFTED, bKeyDown);
               SetKeyState(VK_LCONTROL, KEY_DOWN);
               SendKey(VK_LEFT,   UNSHIFTED, bKeyDown);
               SendKey(VK_RIGHT,  SHIFTED, bKeyDown);
               SendKey(VK_INSERT, UNSHIFTED, KEY_DOWN);
               SendKey(VK_INSERT, UNSHIFTED, KEY_UP);
               return INVISIBLE_KEYSTROKE;
               }
            else
              //key up, don't transmit ^C keyup, lest we get into infinite loop:
              return INVISIBLE_KEYSTROKE;
            }
          break; //Probably superfluous, given that all the branches of the if's
                 //have a return...

        case VK_D: // ^D = down 7 lines
          ZeroCounts();
          if (bKeyDown)
            {int i;
             SetKeyState(VK_LCONTROL, KEY_UP);
             for (i = 1; i <= 7; i++)
                {
                SendKey(VK_DOWN, g_bAnchorDropped, TRUE);
                SendKey(VK_DOWN, g_bAnchorDropped, FALSE);
                }
            SetKeyState(VK_LCONTROL, KEY_DOWN);
            }
          return INVISIBLE_KEYSTROKE;

        case VK_E: // ^E = end of line; xlate to
                   // PgDn first time, ^PgDn second time, and ^End third time
          if (bKeyDown)
            /*Alter the counts on bKeyDown only:*/
            {
            g_iHomeCount = 0;
            ++g_iEndCount;
            }
          if (g_iEndCount == 1)
            {
            SetKeyState(VK_LCONTROL, KEY_UP);
            SendKey(VK_END, g_bAnchorDropped, bKeyDown);
            SetKeyState(VK_LCONTROL, KEY_DOWN);
            }
          else if (g_iEndCount == 2)
            //Leave Control key down, so we go to bottom of screen
            SendKey(VK_NEXT, g_bAnchorDropped, bKeyDown);
              /*^PG_DN goes to bottom of screen in many apps.  However,
                in Word this needs to be assigned to EndOfWindow (and shift
                ^PG_DN to EndOfWindowExtend), using the Tools | Customize menu
                item.*/
          else if (g_iEndCount > 2) //Should never be > 3, but...
            {//Leave Control key down, so we go to BOF
            g_iEndCount = 0;
            SendKey(VK_END, g_bAnchorDropped, bKeyDown);
            }
          return INVISIBLE_KEYSTROKE;

        case VK_G: // ^G = delete character right
          ZeroCounts();
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_DELETE, UNSHIFTED, bKeyDown);
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          g_bAnchorDropped = FALSE; //turn off anchor, if it was off
          return INVISIBLE_KEYSTROKE;

        case VK_H: // ^H = left
          ZeroCounts();
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_LEFT, g_bAnchorDropped, bKeyDown);
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          return INVISIBLE_KEYSTROKE;

        case VK_J: // ^J = down
          ZeroCounts();
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_DOWN, g_bAnchorDropped, bKeyDown);
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          return INVISIBLE_KEYSTROKE;

        case VK_K: // ^K = up
          ZeroCounts();
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_UP, g_bAnchorDropped, bKeyDown);
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          return INVISIBLE_KEYSTROKE;

        case VK_L: // ^L = right
          ZeroCounts();
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_RIGHT, g_bAnchorDropped, bKeyDown);
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          return INVISIBLE_KEYSTROKE;

        case VK_M: // ^M = page up
          ZeroCounts();
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_PRIOR, g_bAnchorDropped, bKeyDown);
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          return INVISIBLE_KEYSTROKE;

        case VK_N: // ^N = page down
          ZeroCounts();
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_NEXT, g_bAnchorDropped, bKeyDown);
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          return INVISIBLE_KEYSTROKE;

        case VK_Q: // ^Q = toggle anchor
          ZeroCounts();
          if (bKeyDown)
            g_bAnchorDropped = !g_bAnchorDropped;
          return INVISIBLE_KEYSTROKE;

        case VK_R: // ^R = find again
          ZeroCounts();
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_F3, UNSHIFTED, bKeyDown);
          /*This won't work for everything, but for those programs that have
            SearchAgain assigned to a keystroke, F3 seems fairly standard*/
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          return INVISIBLE_KEYSTROKE;

        case VK_U: // ^U = up 7 lines
          ZeroCounts();
          if (bKeyDown)
              {
              int i;
              SetKeyState(VK_LCONTROL, KEY_UP);
              for (i = 1; i <= 7; i++)
                {
                SendKey(VK_UP, g_bAnchorDropped, TRUE);
                SendKey(VK_UP, g_bAnchorDropped, FALSE);
                }
              SetKeyState(VK_LCONTROL, KEY_DOWN);
              }
          return INVISIBLE_KEYSTROKE;

        /*No special handling needed for ^V = paste*/

        case VK_W: // ^W = word right
          ZeroCounts();
          SendKey(VK_RIGHT, g_bAnchorDropped, bKeyDown);
          return INVISIBLE_KEYSTROKE;

        case VK_X: // ^X = cut; if nothing is selected, we cut the current word
                   // (Cf. ^C).  We do not pass on the ^X, lest we get into an infinite loop.
          ZeroCounts();
          if (IsMatchingWindowName(EXCEL_MAIN_NAME)) //Excel (main pane, no slxn)
            {
            g_bAnchorDropped = FALSE;
            return VISIBLE_KEYSTROKE; //Allow the program to see the ^X
            }
          if (g_bAnchorDropped)
            {//There is a slxn, so copy it, then delete it
            g_bAnchorDropped = FALSE;
            if (bKeyDown)
              {//Copy the slxn with a ^INS:
              SendKey(VK_INSERT, UNSHIFTED, KEY_DOWN);
              SendKey(VK_INSERT, UNSHIFTED, KEY_UP);
              //...and delete it:
              SetKeyState(VK_LCONTROL, KEY_UP);
              SendKey(VK_DELETE, UNSHIFTED, KEY_DOWN);
              SendKey(VK_DELETE, UNSHIFTED, KEY_UP);
              //Reset the CTRL key to its original state:
              SetKeyState(VK_LCONTROL, KEY_DOWN);
              }
            return INVISIBLE_KEYSTROKE;
            }
          else
            {if (bKeyDown)
               {//Select current word: <right><ctrl-left><ctrl-shift-right>
               SetKeyState(VK_LCONTROL, KEY_UP);
               SendKey(VK_RIGHT,  UNSHIFTED, bKeyDown);
               SetKeyState(VK_LCONTROL, KEY_DOWN);
               SendKey(VK_LEFT,   UNSHIFTED, bKeyDown);
               SendKey(VK_RIGHT,  SHIFTED, bKeyDown);
               //Copy this slxn with ^INS:
               SendKey(VK_INSERT, UNSHIFTED, KEY_DOWN);
               SendKey(VK_INSERT, UNSHIFTED, KEY_UP);
               //...and delete it:
               SetKeyState(VK_LCONTROL, KEY_UP);
               SendKey(VK_DELETE, UNSHIFTED, KEY_DOWN);
               SendKey(VK_DELETE, UNSHIFTED, KEY_UP);
               /*Note: for reasons which are unclear, Word seems to see the above
                 as a CTRL-DELETE, no matter how many times you set VK_LCONTROL
                 and/or VK_CONTROL to KEY_UP.  So ensure that CTRL-DELETE is set
                 to EditClear in Word (its default value is DeleteWord).*/
               //Reset the CTRL key to its original state:
               SetKeyState(VK_LCONTROL, KEY_DOWN);
               }
            return INVISIBLE_KEYSTROKE;
            }
          break;  /*Probably superfluous, given that one or the other of
                    the returns above must be taken*/

        case VK_Y: // ^Y = delete line
            ZeroCounts();
            SetKeyState(VK_LCONTROL, KEY_UP);
            SendKey(VK_HOME,   UNSHIFTED, bKeyDown);  //boln
            SendKey(VK_END,      SHIFTED, bKeyDown);  //select to eoln
            SendKey(VK_RIGHT,    SHIFTED, bKeyDown);  //select newline
            SendKey(VK_DELETE, UNSHIFTED, bKeyDown);  //cut
            SetKeyState(VK_LCONTROL, KEY_DOWN);
            return INVISIBLE_KEYSTROKE;

        /*No special handling needed for ^Z = undo*/

        case VK_BACK: //^BKSP = delete word left
          ZeroCounts();
          g_bAnchorDropped = FALSE;
          /*If the cursor is in the middle of a word, we only delete that
            portion of the word to the left of the cursor.  But we first
            wiggle the cursor once, to turn off any existing marking: */
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_LEFT,  UNSHIFTED, bKeyDown);      //wiggle left
          SendKey(VK_RIGHT, UNSHIFTED, bKeyDown);      //wiggle right
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          SendKey(VK_LEFT,  SHIFTED, bKeyDown);        //select to beginning of word
          SetKeyState(VK_LCONTROL, KEY_UP);
          SendKey(VK_DELETE, UNSHIFTED, bKeyDown);     //...and delete it
          SetKeyState(VK_LCONTROL, KEY_DOWN);
          return INVISIBLE_KEYSTROKE;

        default:  //Not a key we translate
          break;

        }   //Case wVirtKeyCode
      }  //If it's a Ctrl key
    } //If nCode >= 0

  /*We get here if it wasn't a key we translate (otherwise, we would have
    already returned).  Set toggles and counts to FALSE/Zero, except for key codes
    generated by above code for ^A and ^E: */
  if (  (wVirtKeyCode != VK_HOME)
     && (wVirtKeyCode != VK_END)
     && (wVirtKeyCode != VK_PRIOR)
     && (wVirtKeyCode != VK_NEXT)
     )
    ZeroCounts();

  if ((wVirtKeyCode == VK_MENU) || (wVirtKeyCode == VK_INSERT))
     /*If <ALT> key is pressed, turn off anchor (so we're not selecting in dlg
       box, etc.)  Previously, I was also turning it off for alphanumeric keys.
       But this was sometimes turning off the anchor in Mozilla Thunderbird
       (and probably Powerpoint).  Might want to do this for <BKSP> (VK_BACK),
       <DEL> (VK_DELETE), and maybe some others.  But do NOT do that for ALL keys,
       because this "sees" the up/down etc. cursor key msgs its own code produces.
     */
     g_bAnchorDropped = FALSE;

  return(CallNextHookEx(NULL, nCode, wVirtKeyCode, lKeystrokeInfo));
}


BOOL WINAPI __declspec(dllexport) DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
/*Initialize the DLL.  Actually, this seems to get called fairly often (every
  time there's a focus shift?), and there's nothing to initialize.
*/
{
  return TRUE;   // Initialization is successful
}
