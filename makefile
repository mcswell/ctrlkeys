#Makefile for Control Key Remapper

CC = /mnt/c/msys64/mingw64/bin/cc.exe
RC = /mnt/c/msys64/mingw64/bin/windres.exe

EXE_LIBS = kernel32.lib user32.lib gdi32.lib shell32.lib
DLL_LIBS = user32.lib

all: CtrlKeys.exe

install: CtrlKeys.exe sendkeys.dll
#Copy executables to start location (assumed to be next dir up).
# This will fail if this program is already running.
	cp $^ ..

CtrlKeys.exe: CtrlKeys.c sendkeys.dll resource.o
	$(CC) $(CFLAGS) -o $@ $< resource.o

sendkeys.dll: sendkeys.c sendkeys.h CtrlKeys.h
	$(CC) -shared -o $@ sendkeys.c

resource.o: resource.rc
	$(RC) -i $< -o $@

clean:
	-$(RC) $< $@
	-$(RM) CtrlKeys.exe
	-$(RM) resource.o
	-$(RM) sendkeys.dll
