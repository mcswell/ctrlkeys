/****************************************************************************
CtrlKeys.c
        PROGRAM: CtrlKeys.c

        PURPOSE: Control key remapping for all windows programs.  The mapping is
                 hard-coded in sendkeys.c

        FUNCTIONS:
                WinMain() - calls initialization function, processes message loop
                InitApplication() - initializes window data and registers window
                InitInstance() - saves instance handle and creates main window
                MainWndProc() - processes messages
                Menu() - processes Menu msgs
                ToggleTranslation() - turns translation by sendkeys on and off
****************************************************************************/

#define OEMRESOURCE //Debug: for icon
#include <windows.h>
#include <shellapi.h>
#include "CtrlKeys.h"

HANDLE hInst;
HANDLE hHookDll, hSystemHook; /* handle of the DLL and of the SysHook;
                                 if hSystemHook is NULL, keyboard is not hooked*/
HOOKPROC hHookProc;
NOTIFYICONDATA TrayNotifyStruct;       /* icon to display in the Sys Tray */
HMENU    TestMenu;


/*-----------------COMMAND LINE PARAMETERS (none)*/
//extern int      __argc;
//extern char **  __argv;
//extern char *   envp[];

extern DWORD FAR HKKeyHook(int nCode, WORD wVirtKeyCode, DWORD lKeystrokeInfo);


int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, int nCmdShow)
/****************************************************************************
Calls initialization function, processes message loop.

Windows recognizes this function by name as the initial entry point
for the program.  If no other instance of the program is running,
this function calls the application initialization routine, and always
calls the instance initialization routine.  It then executes a message
retrieval and dispatch loop that is the top-level control structure
for the remainder of execution.  The loop is terminated when a WM_QUIT
message is received, at which time this function exits the application
instance by returning the value passed by PostQuitMessage().

If this function must abort before entering the message loop, it
returns the conventional value NULL.

The nCmdShow arg is ignored; we do not show the window.
****************************************************************************/
{
  MSG msg;  /* message*/
  HANDLE hMutex;

  /*Don't run two copies.  Under Win32, hPrevInstance no longer works,
    so use Mutex.  (We could, but don't, do error checking here.)*/
  hMutex = CreateMutex(NULL, TRUE, "Control key remapper");
  if (GetLastError())
     {MessageBox(NULL, "Control key remapper is already running.", "Control", MB_OK | MB_ICONWARNING);
      return (FALSE);
     }

  if (!InitApplication(hInstance))    /* Initialize shared things*/
     return (FALSE);                  /* Exits if unable to initialize*/
  /*Initialize the instance, including starting the DLL: */
  if (!InitInstance(hInstance))
    return (FALSE);

  /*Acquire and dispatch messages until a WM_QUIT message is received: */
  while (GetMessage(&msg,              /* message structure */
                    NULL,              /* handle of window receiving the message*/
                    0,                 /* lowest message to examine */
                    0))                /* highest message to examine*/
    {
    TranslateMessage(&msg);            /* Translates virtual key codes */
    DispatchMessage(&msg);             /* Dispatches message to window */
    }

  /*When we get here, we're quitting.  Turn off the system hook and the DLL: */
  UnhookWindowsHookEx(hSystemHook);
  FreeLibrary(hHookDll);
  /*Get rid of the icon in the system tray: */
  //FIX: not disappearing
  Shell_NotifyIcon(NIM_DELETE, &TrayNotifyStruct);
  /*..and release the mutex: */
  ReleaseMutex(hMutex);

  return (msg.wParam);    /* Returns the value from PostQuitMessage*/
}



BOOL InitApplication(HANDLE hInstance)
/****************************************************************************
Initializes window data and registers window class

This function is called at initialization time only if no other
instances of the application are running.  This function performs
initialization tasks that can be done once for any number of running
instances.  We initialize a window class by filling out a data
structure of type WNDCLASS and calling the Windows RegisterClass()
function. Since all instances of this application use the same window
class, we only need to do this when the first instance is initialized.
****************************************************************************/
{
  WNDCLASS  wc;

  wc.style         = SW_HIDE;            /*Class style: hide the window*/
  wc.lpfnWndProc   = MainWndProc;        /*Function to retrieve messages*/
  wc.cbClsExtra    = 0;                  /*No per-class extra data*/
  wc.cbWndExtra    = 0;                  /*No per-window extra data*/
  wc.hInstance     = hInstance;          /*Application that owns the class*/
  wc.hIcon         = LoadIcon(hInstance, "ARROWS_ICON");
  wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
  wc.hbrBackground = WHITE_BRUSH;
  wc.lpszMenuName  = "ControlKeyMenu";   /*Name of menu resource in .RC file*/
  wc.lpszClassName = "ControlKeyWClass"; /*Name used in call to CreateWindow*/

  //Prevent a useless console window from showing up:
  ShowWindow(GetConsoleWindow(), SW_HIDE);
  //Register the window class and return success/failure code:
  return (RegisterClass(&wc));
}


BOOL InitInstance(HANDLE hInstance)
/****************************************************************************
Saves instance handle and creates main window

This function is called at initialization time for every instance of
this application.  This function performs initialization tasks that
cannot be shared by multiple instances.

In this case, we save the instance handle in a static variable and
create and display the main program window.
****************************************************************************/
{
  HWND     hWnd;                  /* Main window handle*/

  /* Save the instance handle in static variable, which will be used in
     many subsequent calls from this application to Windows: */
  hInst = hInstance;

  /*Create a pop-up window to be displayed when the tray icon is right-clicked: */
  hWnd = CreateWindow
     ("ControlKeyWClass",         /* See RegisterClass() call*/
      "Control Keys",                     /* Text for window title bar  */
      WS_POPUPWINDOW | WS_CAPTION,/* Window style*/
      200,                        /* Default horizontal position for pop-up*/
      1000,                       /* Default vertical position for pop-up*/
      200,                        /* Default width  */
      60,                         /* Default height */
      NULL,                       /* Overlapped windows have no parent*/
      NULL,                       /* Use the window class menu  */
      hInstance,                  /* This instance owns this window*/
      NULL                        /* Pointer not needed*/
  );

  /* If window could not be created, return "failure" */
  if (!hWnd)
    return (FALSE);

  /*The window goes in the systray, so we don't make it visible,
    but this is how you would do so if you wanted to:*/
 // ShowWindow(hWnd, SW_MINIMIZE);
 // UpdateWindow(hWnd);
  /*Show the system tray icon: */
  TrayNotifyStruct.cbSize = sizeof(NOTIFYICONDATA);
  ZeroMemory(&TrayNotifyStruct, sizeof(NOTIFYICONDATA));
  TrayNotifyStruct.hWnd = hWnd;
  TrayNotifyStruct.uID = 1;
  TrayNotifyStruct.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
  TrayNotifyStruct.uCallbackMessage = IDM_TRAY_MSG;
  TrayNotifyStruct.hIcon = LoadIcon(hInstance, "ARROWS_ICON");

  lstrcpy(TrayNotifyStruct.szTip, "Control key remapper");
  Shell_NotifyIcon(NIM_ADD, &TrayNotifyStruct);

  /*Start the DLL: */
  hHookDll = LoadLibrary("sendkeys");

  /*Install keyboard hook to trap all keyboard messages NB: "HKKeyHook" must
    be preceded by an underscore for the lcc compiler, but NOT for the MS Visual
    C compiler, nor for the msys64 compiler!*/
  hHookProc = (HOOKPROC) GetProcAddress(hHookDll, "HKKeyHook");
  hSystemHook =  SetWindowsHookEx(WH_KEYBOARD, hHookProc, hHookDll, 0);

  return (TRUE);        /* Returns the value from PostQuitMessage */
}


LRESULT CALLBACK MainWndProc(HWND hWnd, unsigned message, WPARAM wParam, LPARAM lParam)
/****************************************************************************
Process one of several messages:
  WM_CREATE
  IDM_TRAY_MSG      - Display the menu
  WM_COMMAND        - Command from menu
  WM_DESTROY        - Destroy window
  WM_CLOSE
****************************************************************************/
{
  switch (message) {
    case WM_CREATE:
      break;

    case IDM_TRAY_MSG:
     if (lParam == WM_RBUTTONDOWN || lParam == WM_LBUTTONDBLCLK)
        {
        DialogBox(hInst,                       /*current instance*/
                   "MenuBox",                  /*resource to use*/
                   hWnd,                       /*parent handle*/
                   (DLGPROC) ControlKeyMenu);  /*Menu() instance address*/
        break;
        }
     else                             /*Let Windows process it*/
       return (DefWindowProc(hWnd, message, wParam, lParam));

    case WM_COMMAND:  /*Message: command from application menu */
      if (wParam == IDM_CLOSE)
         {PostQuitMessage(0);
          break;
         }
      else            /*Let Windows process it*/
        return (DefWindowProc(hWnd, message, wParam, lParam));

    case WM_DESTROY:  /* message: window being destroyed*/
      PostQuitMessage(0);
      break;

    case WM_CLOSE:
      return (DefWindowProc(hWnd, message, wParam, lParam));
      break;

    default:          /* Passes it on if unproccessed*/
      return (DefWindowProc(hWnd, message, wParam, lParam));
  }
  return (0);
}


DLGPROC ControlKeyMenu(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
/****************************************************************************
Processes messages for the dialog box
    MESSAGES: WM_INITDIALOG - initialize dialog box
              WM_COMMAND        - Input received
No initialization is needed for this particular dialog box, but TRUE must be
returned to Windows at initialization.
****************************************************************************/
{
  switch (message) {
     case WM_INITDIALOG:             /*message: initialize dialog box*/
       return (DLGPROC) TRUE;

     case WM_CLOSE:                  /*User clicked the "Exit" button*/
       EndDialog(hDlg, TRUE);        /*Exits the dialog box*/
       return (DLGPROC) TRUE;
       break;

     case WM_COMMAND:                /*message: received a command*/
       if (wParam == IDM_CLOSE)
          PostQuitMessage(0);
       else if (wParam == IDM_TOGGLE)
          {
            ToggleTranslation();
            EndDialog(hDlg, TRUE);   /*Exits the dialog box*/
          }
       else if (wParam == IDOK)
          EndDialog(hDlg, TRUE);     /*Exits the dialog box*/
       return (DLGPROC) TRUE;
       break;
  }
  return (DLGPROC) FALSE;            /*Didn't process a message*/
}


void ToggleTranslation()
/*****************************************************************************
 If translation is on, turn it off, else turn it on.
 ****************************************************************************/
{
  if (hSystemHook)
    {
      UnhookWindowsHookEx(hSystemHook);
      hSystemHook = NULL;
    }
  else
    hSystemHook = SetWindowsHookEx(WH_KEYBOARD, hHookProc, hHookDll, 0);
}