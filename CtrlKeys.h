#define IDM_TRAY_MSG 101
#define IDM_TOGGLE   102
#define IDM_CLOSE    103

int PASCAL WinMain(HINSTANCE, HINSTANCE, LPSTR, int);
BOOL InitApplication(HANDLE);
BOOL InitInstance(HANDLE);
LRESULT CALLBACK MainWndProc(HWND, unsigned, WPARAM, LPARAM);
DLGPROC ControlKeyMenu(HWND, UINT, WPARAM, LPARAM);
void ToggleTranslation();